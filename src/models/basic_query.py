import graphene


class Query(graphene.ObjectType):
    hello = graphene.String()

    def resolve_hello(self, info):
        return "Hola, mundo!"


basic_schema = graphene.Schema(query=Query)
