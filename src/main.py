from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp
from src.models.basic_query import basic_schema

app = FastAPI()

app.add_route("/graphql", GraphQLApp(schema=basic_schema))


@app.get("/")
def read_root():
    return "Example Fastapi , graphql and Poetry"
